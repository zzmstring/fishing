#ifndef Grid_h__
#define Grid_h__
#include "core/define.h"
#include "core/AStarNode.h"
#include <string>
#include <vector>

const int straightOffsets[4][2]={
	{1,0}, //[[W]] 
	{-1,0}, //[[E]]
	{0,1}, //[[S]] 
	{0,-1} //[[N]]
};

const int diagonalOffsets[4][2]={
	{ -1, -1}, //[[NW]]
	{ 1, -1},  //[[NE]]
	{ -1,  1}, //[[SW]]
	{ 1,  1}  //[[SE]]
};


class Grid
{
public:
	Grid(void);
	~Grid(void);

	// bool initWithFile(const char* mapFilePath,int8 blockVal);
	
	// bool initWithString(std::string& str);

	bool initWithTalbe( const char* tb,int8 _blockVal,int width,int height );
	// bool initWithTalbe(int8** tb,int8 blockVal,int width,int height);

	void setNodeAt(int index, int value);

	void setBlockVal(int val);

	bool isWalkAbleAt(uint32 x,uint32 y);

	bool outOfMap(uint32 x,uint32 y);

	uint32 getWidth(){ return width; }
	uint32 getHeight(){ return height; }

	// int8** getMap(){ return map; }
	AStarNode*** getNodes(){ return nodes; }
	AStarNode* getNodeAt(int x,int y);

	void getNeighbours(std::vector<AStarNode*>& neighbours,AStarNode* node,bool allowDiagonal,bool tunnel);
private:
	bool initNodes();

private:
	// int8** map;
	const char* map;
	uint32 width;
	uint32 height;
	AStarNode*** nodes;
	int8 blockVal;
};

#endif // Grid_h__
