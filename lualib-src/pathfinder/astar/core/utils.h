#ifndef utils_h__
#define utils_h__

#include "define.h"
#include <string>
#include <vector>

using namespace std;

class utils
{
public:
	utils(void);
	~utils(void);


	static int8** stringMapToArray(const std::string& str,uint32& width,uint32& height);
	
	static void split( const std::string & str, std::vector< std::string > & result, const std::string & sep );
	
	static void split_withspace( const std::string & str, std::vector< std::string > & result );


};

#endif // utils_h__
