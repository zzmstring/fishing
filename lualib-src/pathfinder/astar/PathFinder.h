#ifndef PathFinder_h__
#define PathFinder_h__
#include "core/AStarNode.h"
#include "Grid.h"
#include "core/Path.h"
#include <vector>
using namespace std;

enum FindersMode
{
	eFindersMode_AStar,
	eFindersMode_DIJKStar,
	eFindersMode_ThetaStar,
	eFindersMode_BFS,
	eFindersMode_DFS,
	eFindersMode_JPS,
};

enum SearchMode
{
	eSearchMode_Diagonal,		//8-directions
	eSearchMode_Orthogonal,		//4-directions
};

enum HeuristicMode
{
	eHeuristicMode_Manhattan,
	eHeuristicMode_Cardintcard,
};


struct lua_State;
class AStar;
class PathFinder
{
	friend class AStar;
	friend class ThataStar;
public:
	PathFinder(void);
	~PathFinder(void);

	// call init after this constructor
	// PathFinder(const char* mapPath,int8 blockVal);

	bool init( FindersMode fm,SearchMode sm,HeuristicMode hm,bool tunnel);
	void setMapData(const char* data,int block,int w,int h );
	// void setMapData(int8** data,int block,int w,int h);

	//call in lua only,use the obj new with default constructor
	//bool initWithLuaTable(lua_State* L,int width,int height,int blockVal,FindersMode fm,SearchMode sm,HeuristicMode hm,bool tunnel);

	Path* getPath(int startX,int startY,int endX,int endY);

	int getHeuristic(AStarNode* sn,AStarNode* en);

	Grid* getGrid(){ return grid; }

	void setGrid(Grid* g){ grid  = g; }

	int getWalkable();

	void reset();

private:
	Path* traceBackPath(AStarNode* endNode,AStarNode* startNode);

private:
	Grid* grid;
	bool allowDiagonal;
	bool tunnel;
	FindersMode findMode;
	HeuristicMode heuristic;
	vector<AStarNode*> toClear;
	AStar* finder;

	Path* recentlyPath;
};



struct PathFinderGC
{
	PathFinder* _gcObj;
};

#endif // PathFinder_h__
